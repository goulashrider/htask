<?php
/**
 * Created by PhpStorm.
 * User: zpalasthy
 * Date: 15/02/2018
 * Time: 18:49
 */

namespace App;

class XmlHandler {

    public $file;
    public $xml;

    public function __construct ( $file ) {

        $this->file = $file;
        $this->xml = simplexml_load_file( $this->file );
    }

    public function write () {

    }

}