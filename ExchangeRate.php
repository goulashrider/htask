<?php
/**
 * Created by PhpStorm.
 * User: zpalasthy
 * Date: 15/02/2018
 * Time: 18:49
 */

namespace App;

class ExchangeRate {

    public $values;

    public function __construct ( \SimpleXMLElement $rates ) {

        foreach ( $rates as $rate ) {
            foreach ( $rate as $r ) {
                $this->values[ (string) $rate->attributes()[ 'date' ] ][ (string) $r->attributes()[ 'code' ] ] = (string) $r->attributes()[ 'value' ];
            }
        }
    }
}