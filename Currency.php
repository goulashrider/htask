<?php
/**
 * Created by PhpStorm.
 * User: zpalasthy
 * Date: 15/02/2018
 * Time: 18:50
 */

namespace App;

class Currency {

    public $name;
    public $code;
    public $rateHistory;

    public function __construct ( \SimpleXMLElement $element ) {

        $this->name = (string) $element->name;
        $this->code = (string) $element->code;

        foreach ( $element->rateHistory as $hist ) {
            $this->rateHistory = new ExchangeRate( $hist );
        }
    }

}