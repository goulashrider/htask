<?php
/**
 * Created by PhpStorm.
 * User: zpalasthy
 * Date: 15/02/2018
 * Time: 18:49
 */

namespace App;

class Order {

    public $id;
    public $currency;
    public $date;
    public $products;

    public function __construct ( \SimpleXMLElement $order ) {

        $this->id       = (string) $order->id;
        $this->currency = (string) $order->currency;
        $this->date     = new \DateTime( (string) $order->date );

        foreach ( $order->products->product as $p ) {

            $this->products[] = new Product( $p );

        }

    }

    public function convert () {

    }

}