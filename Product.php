<?php
/**
 * Created by PhpStorm.
 * User: zpalasthy
 * Date: 15/02/2018
 * Time: 18:49
 */

namespace App;

class Product {

    public $title;
    public $price;

    public function __construct ( \SimpleXMLElement $product ) {

        $this->title = (string) $product->attributes()[ 'title' ];
        $this->price = (double) $product->attributes()[ 'price' ];
    }

}