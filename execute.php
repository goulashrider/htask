<?php
/**
 * Created by PhpStorm.
 * User: zpalasthy
 * Date: 15/02/2018
 * Time: 18:55
 */

include_once 'Currency.php';
include_once 'ExchangeRate.php';
include_once 'Order.php';
include_once 'Product.php';
include_once 'XmlHandler.php';

$exchangerates = new \App\XmlHandler("xml/ExchangeRates.xml");
$orders = new \App\XmlHandler('xml/orders.xml');

$order = $orders->xml;
$rates = $exchangerates->xml;

foreach ($order as $o) {
    $temp = new \App\Order($o);

    print_r($temp);

}

foreach ( $rates as $ex) {
    $temp = new \App\Currency($ex);
//    $temp = new \App\ExchangeRate($ex);
//    $temp = $ex;

    print_r($temp);
}