##Tasks


- Convert the currency of order (id = 1) into EUR
- Convert the currency of order (id = 2) into GBP

- You should output the results in an appropriately structured XML file.



##ACCEPTANCE CRITERIA

####Feature: Calculating exchange rates on orders
In order to charge customers in their local currencies
As a customer

I need to see prices converted to my chosen currency